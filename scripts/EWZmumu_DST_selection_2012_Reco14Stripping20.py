#######################################################################
#######################################################################
##
## Stripping for 2012 electroweak data Z->mumu line
##
#######################################################################
#######################################################################


########################################################################
#
# Set up environment
#
########################################################################
from os import environ
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from Configurables import (FilterDesktop,
                           GaudiSequencer,
                           EventSelector,
                           DaVinci,
                           MicroDSTWriter,
                           SelDSTWriter,
                           )
from Configurables import DaVinci,HltSelReportsDecoder,HltVertexReportsDecoder,HltDecReportsDecoder
from PhysSelPython.Wrappers import Selection, DataOnDemand, SelectionSequence
from CommonParticles.Utils import *
from Configurables import LoKi__HDRFilter as StripFilter


########################################################################
#
# Set the strip filter (change for appropriate HLT lines)
#
########################################################################
sf = StripFilter( 'StripPassFilter', Code="(HLT_PASS('StrippingZ02MuMuLineDecision'))", Location="/Event/Strip/Phys/DecReports" )

getMuons = DataOnDemand( Location = '/Event/Phys/StdNoPIDsPions/Particles' )

ChooseMuon = FilterDesktop('ChooseMuon', Code = 'ALL')

EWSel = Selection(name = 'EWSel', Algorithm = ChooseMuon, RequiredSelections = [getMuons])

MySelSequence = SelectionSequence("ZMUMULINE",TopSelection = EWSel )


###########################################################################
#
# Write it into a DST
#
###########################################################################
dstWriter = SelDSTWriter("MyDSTWriter",
                         SelectionSequences = [MySelSequence],
                         OutputFileSuffix = 'EWZmumu'
                         )
DSTSeq = dstWriter.sequence()


##########################################################################
#
# DaVinci instructions
#
##########################################################################
DaVinci().EventPreFilters = [ sf ]
DaVinci().EvtMax = -1
DaVinci().DataType = "2012"
DaVinci().Simulation = False
DaVinci().WriteFSR = True
DaVinci().Lumi = True
DaVinci().SkipEvents = 0
DaVinci().PrintFreq = 1000
DaVinci().UserAlgorithms = [ DSTSeq ]


####
