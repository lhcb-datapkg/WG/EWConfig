from Gaudi.Configuration import *
from Configurables import LoKi__HDRFilter
from Configurables import DaVinci, SelDSTWriter, GaudiSequencer
from PhysSelPython.Wrappers import AutomaticData, SelectionSequence, EventSelection
sf = LoKi__HDRFilter( 'StripPassFilter', Code="HLT_PASS('StrippingLowMultPhotonLineDecision')", Location="/Event/Strip/Phys/DecReports" )
sel = EventSelection("MyFilter", Algorithm = sf)
ss = SelectionSequence("Low_Mult_2011", TopSelection = sel)
from Configurables import SelDSTWriter
dstWriter = SelDSTWriter("MyDSTWriter", SelectionSequences = [ ss ])
DaVinci().UserAlgorithms = [ dstWriter.sequence() ]
DaVinci ( DataType = '2011' )
DaVinci().PrintFreq = 10000
DaVinci().Simulation = False
DaVinci().EvtMax = -1
