#######################################################################
#######################################################################
##
## Stripping for 2011 electroweak data high pt mu lines
##
#######################################################################
#######################################################################


########################################################################
#
# Set up environment
#
########################################################################
from os import environ
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from Configurables import (FilterDesktop,
                           GaudiSequencer,
                           EventSelector,
                           DaVinci,
                           MicroDSTWriter,
                           SelDSTWriter,
                           )
from Configurables import DaVinci,HltSelReportsDecoder,HltVertexReportsDecoder,HltDecReportsDecoder
from PhysSelPython.Wrappers import Selection, DataOnDemand, SelectionSequence
from CommonParticles.Utils import *
from Configurables import LoKi__HDRFilter as StripFilter


########################################################################
#
# Set the strip filter (change for appropriate HLT lines)
#
########################################################################
sf = StripFilter( 'StripPassFilter', Code="(HLT_PASS('StrippingWMuLineDecision')) | (HLT_PASS('StrippingZ02MuMuLineDecision')) | (HLT_PASS('StrippingZ02MuMuNoPIDsLineDecision'))", Location="/Event/Strip/Phys/DecReports" )

getMuons = DataOnDemand( Location = '/Event/Phys/StdNoPIDsPions/Particles' )

ChooseMuon = FilterDesktop('ChooseMuon', Code = 'ALL')

EWSel = Selection(name = 'EWSel', Algorithm = ChooseMuon, RequiredSelections = [getMuons])

MySelSequence = SelectionSequence("EWHIGHPTMULINES",TopSelection = EWSel )


###########################################################################
#
# Write it into a DST
#
###########################################################################
dstWriter = SelDSTWriter("MyDSTWriter",
                         SelectionSequences = [MySelSequence],
                         OutputFileSuffix = 'EWData'
                         )
DSTSeq = dstWriter.sequence()


##########################################################################
#
# DaVinci instructions
#
##########################################################################
DaVinci().EventPreFilters = [ sf ]
#DaVinci().EvtMax = -1
DaVinci().EvtMax = 1000
DaVinci().DataType = "2011"
DaVinci().Simulation = False
DaVinci().WriteFSR = True
DaVinci().Lumi = True
DaVinci().SkipEvents = 0
DaVinci().PrintFreq = 1000
DaVinci().UserAlgorithms = [ DSTSeq ]


####
